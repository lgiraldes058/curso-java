package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.DefaultComboBoxModel;

public class Categoria_Autos {

	private JFrame frame;
	private JTextArea textArea;
	private JComboBox comboBox;
	private String strCar = "";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Categoria_Autos window = new Categoria_Autos();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Categoria_Autos() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().setForeground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Categoria de los autos");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblNewLabel.setBounds(49, 11, 336, 37);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblCaracteristicasDelAuto = new JLabel("Caracteristicas del Auto:");
		lblCaracteristicasDelAuto.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblCaracteristicasDelAuto.setBounds(10, 89, 172, 19);
		frame.getContentPane().add(lblCaracteristicasDelAuto);

		JLabel lblCategoriaDelAuto = new JLabel("Categoria del Auto:");
		lblCategoriaDelAuto.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblCategoriaDelAuto.setBounds(77, 59, 124, 19);
		frame.getContentPane().add(lblCategoriaDelAuto);
		
		comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			char cCat = ((CharSequence) comboBox.getSelectedItem()).charAt(0);
				switch (cCat) {
			case 'A':
				strCar = "4 Ruedas"+"\n"+"Motor";
				break;
			case 'B':
				strCar = "4 Ruedas"+"\n"+"Motor"+"\n"+"Cierre Centralizado"+"\n"+"Aire";
				break;
			case 'C':
				strCar = "4 Ruedas"+"\n"+"Motor"+"\n"+"Cierre Centralizado"+"\n"+"Aire"+"\n"+"Airbag";
				break;
			}
			textArea.setText(strCar);
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"", "A", "B", "C"}));
		comboBox.setBounds(215, 59, 53, 20);
		frame.getContentPane().add(comboBox);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(122, 119, 218, 132);
		frame.getContentPane().add(textArea);
	}
}
