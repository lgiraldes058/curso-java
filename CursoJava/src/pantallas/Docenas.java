package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Docenas {

	private JFrame frame;
	private JTextField textField;
	private JLabel lblRes;
	private JButton btnCalcular;
	private JButton btnLimpiar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Docenas window = new Docenas();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Docenas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblDocenas = new JLabel("Docenas");
		lblDocenas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblDocenas.setBounds(161, 11, 112, 37);
		frame.getContentPane().add(lblDocenas);
		
		JLabel lblIngreseElNumero = new JLabel("Ingrese el Numero:");
		lblIngreseElNumero.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblIngreseElNumero.setBounds(32, 59, 172, 25);
		frame.getContentPane().add(lblIngreseElNumero);
		
		textField = new JTextField();
		textField.setBounds(214, 61, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		lblRes = new JLabel("");
		lblRes.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblRes.setHorizontalAlignment(SwingConstants.CENTER);
		lblRes.setBounds(10, 119, 414, 37);
		frame.getContentPane().add(lblRes);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			int inro = Integer.parseInt(textField.getText());
			if(inro>36 || inro<1){
				lblRes.setText(inro+" esta fuera de rango");
			}else if(inro<13){
				lblRes.setText(inro+" pertenece a la primera docena");
			}else if(inro>24){
				lblRes.setText(inro+" pertenece a la tercera docena");
			}else
				lblRes.setText(inro+" pertenece a la segunda docena");
			}
		});
		btnCalcular.setBounds(172, 180, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblRes.setText("");
				textField.setText("");
				}
		});
		btnLimpiar.setBounds(172, 217, 89, 23);
		frame.getContentPane().add(btnLimpiar);
	}

}
