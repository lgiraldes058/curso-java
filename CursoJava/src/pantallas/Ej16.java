package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.DropMode;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.Color;

public class Ej16 {

	private JFrame frame;
	private String values[]=new String[11];
	private String strTotal="";
	private JComboBox cmbTabla;
	private JTextPane textPaneTablaResult;
	private JList list;
	private JScrollPane scrollPane;
	private JList list_1;
	private JLabel lblFila;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej16 window = new Ej16();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ej16() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 543, 511);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Tablas de multiplicar");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
		lblNewLabel.setBounds(103, 33, 296, 39);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblElijaTabla = new JLabel("Elija tabla");
		lblElijaTabla.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		lblElijaTabla.setBounds(45, 110, 91, 21);
		frame.getContentPane().add(lblElijaTabla);
		
		String strListTabalas[] = new String [10];
		for(int i=0;i<10;i++){
			strListTabalas[i] = Integer.toString(i+1);
		}
		
		lblFila = new JLabel("");
		lblFila.setBackground(Color.GREEN);
		lblFila.setOpaque(true);
		lblFila.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblFila.setBounds(387, 206, 130, 39);
		frame.getContentPane().add(lblFila);
		
		cmbTabla = new JComboBox();
		cmbTabla.setModel(new DefaultComboBoxModel(strListTabalas));
		cmbTabla.setBounds(165, 113, 109, 20);
		frame.getContentPane().add(cmbTabla);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int iTabla = Integer.parseInt((String)cmbTabla.getSelectedItem());
				for(int i=0;i<11;i++){
					values[i]=iTabla+"x"+i+"="+(iTabla*i);
				}
				
				list.setModel(new AbstractListModel() {
					public int getSize() {
						return values.length;
					}
					public Object getElementAt(int index) {
						return values[index];
					}
				});
				list.setFont(new Font("Tahoma", Font.PLAIN, 15));
				scrollPane.setViewportView(list);}
		});
		btnCalcular.setBounds(341, 112, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(150, 163, 227, 226);
		frame.getContentPane().add(scrollPane);
		
		list = new JList();
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				int iFila = e.getLastIndex();
				lblFila.setText(values[iFila]);
				}
		});

		list.setFont(new Font("Tahoma", Font.PLAIN, 15));
		scrollPane.setViewportView(list);
		

		
	}
}
 
