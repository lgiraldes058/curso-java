package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;

public class Ej18_Grilla2 {

	private JFrame frame;
	private JTable table;
	private JComboBox comboBoxHasta;
	private JComboBox comboBoxDesde;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej18_Grilla2 window = new Ej18_Grilla2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ej18_Grilla2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 567, 404);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 351, 366);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null},
				{null, null, null},
				{null, null, null},
			},
			new String[] {
				"Tabla del 1", "Tabla del 2", "Tabla del 3"
			}
		));
		scrollPane.setViewportView(table);
		
		JLabel lblDesde = new JLabel("Desde:");
		lblDesde.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDesde.setHorizontalAlignment(SwingConstants.CENTER);
		lblDesde.setBounds(407, 11, 134, 30);
		frame.getContentPane().add(lblDesde);
		
		JLabel lblHasta = new JLabel("Hasta");
		lblHasta.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblHasta.setHorizontalAlignment(SwingConstants.CENTER);
		lblHasta.setBounds(400, 93, 141, 20);
		frame.getContentPane().add(lblHasta);
		
		comboBoxDesde = new JComboBox();
		comboBoxDesde.setBounds(417, 39, 124, 20);
		frame.getContentPane().add(comboBoxDesde);
		
		comboBoxHasta = new JComboBox();
		comboBoxHasta.setBounds(417, 124, 124, 20);
		frame.getContentPane().add(comboBoxHasta);
	}
}
