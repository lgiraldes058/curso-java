package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Ej19 {

	private JFrame frame;
	private JLabel lblSum;
	private JLabel lblProm;
	private JButton btnCalcular;
	private JLabel lblNum;
	private String strNro="Numeros Elegidos:";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej19 window = new Ej19();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ej19() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 406);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblAleatorios = new JLabel("Aleatorios");
		lblAleatorios.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblAleatorios.setBounds(152, 21, 130, 37);
		frame.getContentPane().add(lblAleatorios);
		
		JLabel lblSuma = new JLabel("Suma:");
		lblSuma.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSuma.setBounds(90, 81, 81, 19);
		frame.getContentPane().add(lblSuma);
		
		JLabel lblPromedio = new JLabel("Promedio");
		lblPromedio.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPromedio.setBounds(90, 105, 81, 19);
		frame.getContentPane().add(lblPromedio);
		
		final Integer inros[] = new Integer [10];
		for(int i=0;i<inros.length;i++){
			inros[i] = (int) Math.floor(Math.random()*101);
		}
		
		lblSum = new JLabel("");
		lblSum.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSum.setBounds(181, 81, 81, 19);
		frame.getContentPane().add(lblSum);
		
		lblProm = new JLabel("");
		lblProm.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblProm.setBounds(181, 105, 81, 19);
		frame.getContentPane().add(lblProm);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Integer iSuma = 0;
				int i = 0;
				while (i<inros.length) {
					iSuma += inros[i];
					lblSum.setText(String.valueOf(iSuma));
					strNro+=" "+inros[i];
					i++;
				}
				float fprom = (float) iSuma/inros.length;
				lblNum.setText(strNro);
				lblProm.setText(String.valueOf(fprom));
			}
		});
		btnCalcular.setBounds(172, 307, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		lblNum = new JLabel("");
		lblNum.setHorizontalAlignment(SwingConstants.CENTER);
		lblNum.setVerticalAlignment(SwingConstants.TOP);
		lblNum.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNum.setBounds(10, 201, 424, 95);
		frame.getContentPane().add(lblNum);
		
	}
}
