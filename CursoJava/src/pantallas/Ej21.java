package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Ej21 {

	private JFrame frame;
	private JLabel lblMax;
	private JLabel lblMin;
	private JButton btnCalcular;
	private JLabel lblNum;
	private String strNro="Numeros Elegidos:";
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej21 window = new Ej21();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ej21() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 406);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblAleatorios = new JLabel("Aleatorios");
		lblAleatorios.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblAleatorios.setBounds(152, 21, 130, 37);
		frame.getContentPane().add(lblAleatorios);
		
		JLabel lblSuma = new JLabel("Maximo:");
		lblSuma.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSuma.setBounds(90, 81, 81, 19);
		frame.getContentPane().add(lblSuma);
		
		JLabel lblPromedio = new JLabel("Minimo:");
		lblPromedio.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPromedio.setBounds(90, 105, 81, 19);
		frame.getContentPane().add(lblPromedio);
		
		final Integer inros[] = new Integer [10];
		for(int i=0;i<inros.length;i++){
			inros[i] = (int) Math.floor(Math.random()*101);
		}
		
		lblMax = new JLabel("");
		lblMax.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblMax.setBounds(181, 81, 81, 19);
		frame.getContentPane().add(lblMax);
		
		lblMin = new JLabel("");
		lblMin.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblMin.setBounds(181, 105, 81, 19);
		frame.getContentPane().add(lblMin);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int iMax = 0;
				int iMin = 100;
				int i = 0;
				strNro = "Numeros Elegidos:";
				do {
				int j = inros[i];
				strNro+=" "+inros[i];
				if(j>iMax){
					iMax = j;
				}else if(j<iMin){
					iMin = j;
				}
				i++;
				} while (i<inros.length);
				lblMin.setText(String.valueOf(iMin));
				lblMax.setText(String.valueOf(iMax));
				lblNum.setText(strNro);
			}
		});
		btnCalcular.setBounds(172, 307, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		lblNum = new JLabel("");
		lblNum.setHorizontalAlignment(SwingConstants.CENTER);
		lblNum.setVerticalAlignment(SwingConstants.TOP);
		lblNum.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNum.setBounds(10, 201, 424, 95);
		frame.getContentPane().add(lblNum);
		
	}
}
