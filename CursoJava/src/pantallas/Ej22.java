package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

public class Ej22 {

	private JFrame frame;
	private JButton btnCalcular;
	private JTable table;
	private JScrollPane scrollPane;
	private String strTabla[][] = new String[10][5];
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej22 window = new Ej22();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ej22() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 570, 369);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblSueldo = new JLabel("Sueldo");
		lblSueldo.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblSueldo.setBounds(232, 11, 90, 37);
		frame.getContentPane().add(lblSueldo);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
			int i = 0;
			String strSueldo = "";
			
			while(i<10){
				
				int iSueldo = (int) Math.floor(Math.random()*80000+20000);
				int iAnt = (int) Math.floor(Math.random()*31);
				int iCat = (int) Math.floor(Math.random()*3+1);
				int iSueldoini = iSueldo;
				String strCat = "";
				
				if(iAnt>0){
				}if(iAnt<6){
					iSueldo = iSueldo+(iSueldo*5/100);
				}else if(iAnt<11){
					iSueldo = iSueldo+(iSueldo*10/100);
				}else{
					iSueldo = iSueldo+(iSueldo*30/100);
				}
				switch (iCat) {
				case 1:
					iSueldo = iSueldo+1000;
					strCat="A";
					break;
				case 2:
					iSueldo = iSueldo+2000;
					strCat="B";
					break;
				case 3:
					iSueldo = iSueldo+3000;
					strCat="C";
					break;
				}
			strTabla[i][0]="Sueldo "+Integer.toString(i+1);
			strTabla[i][1]="$ "+Integer.toString(iSueldo);
			strTabla[i][2]=strCat;
			strTabla[i][3]=Integer.toString(iAnt)+" A�os";
			strTabla[i][4]="$ "+Integer.toString(iSueldoini);
			
			table.setModel(new DefaultTableModel(
					strTabla,
					new String[] {
						"", "Sueldo Final", "Categor\u00EDa", "Antig\u00FCedad", "Sueldo Inicial"
					}
				));
			
			i++;
			}			
			}
		});
		btnCalcular.setBounds(232, 294, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		scrollPane = new JScrollPane();
		scrollPane.setBackground(Color.WHITE);
		scrollPane.setBounds(10, 59, 534, 177);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();

		table.setBackground(Color.WHITE);
		scrollPane.setViewportView(table);
	}
}
