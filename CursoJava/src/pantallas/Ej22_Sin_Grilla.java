package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;

public class Ej22_Sin_Grilla {

	private JFrame frame;
	private JButton btnCalcular;
	private JTextPane textPane;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej22_Sin_Grilla window = new Ej22_Sin_Grilla();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ej22_Sin_Grilla() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 607, 596);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textPane.setBounds(10, 108, 571, 389);
		frame.getContentPane().add(textPane);
		
		JLabel lblSueldo = new JLabel("Sueldo");
		lblSueldo.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblSueldo.setBounds(250, 11, 90, 37);
		frame.getContentPane().add(lblSueldo);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
			int i = 0;
			String strSueldo = "";
			
			while(i<10){
				
				int iSueldo = (int) Math.floor(Math.random()*80000+20000);
				int iAnt = (int) Math.floor(Math.random()*31);
				int iCat = (int) Math.floor(Math.random()*3+1);
				int iSueldoini = iSueldo;
				String strCat = "";
				
				if(iAnt>0){
				}if(iAnt<6){
					iSueldo = iSueldo+(iSueldo*5/100);
				}else if(iAnt<11){
					iSueldo = iSueldo+(iSueldo*10/100);
				}else{
					iSueldo = iSueldo+(iSueldo*30/100);
				}
				switch (iCat) {
				case 1:
					iSueldo = iSueldo+1000;
					strCat="A";
					break;
				case 2:
					iSueldo = iSueldo+2000;
					strCat="B";
					break;
				case 3:
					iSueldo = iSueldo+3000;
					strCat="C";
					break;
				}
			i++;
			strSueldo+="Sueldo Final "+i+": $"+String.valueOf(iSueldo)+" Categoria: "+strCat+" Antig�edad: "+String.valueOf(iAnt)+" A�os Sueldo inicial: $"+String.valueOf(iSueldoini)+"\n\n";	
			}
			textPane.setText(strSueldo);
			
			}
		});
		btnCalcular.setBounds(251, 526, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		JLabel lblSueldosFinales = new JLabel("Sueldos Finales:");
		lblSueldosFinales.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSueldosFinales.setBounds(10, 64, 102, 19);
		frame.getContentPane().add(lblSueldosFinales);
	}
}
