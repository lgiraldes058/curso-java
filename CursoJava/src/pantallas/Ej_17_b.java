package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class Ej_17_b {

	private JFrame frame;
	private JButton btnCalcular;
	private JTextPane textPaneTablaResult;
	private JComboBox comboBox;
	private String strRes = "";
	private JLabel lblLaSumaDe;
	private JLabel lblSuma;
	private Integer iSumaNros=0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej_17_b window = new Ej_17_b();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ej_17_b() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 419);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTablas = new JLabel("Tablas");
		lblTablas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblTablas.setBounds(173, 11, 87, 37);
		frame.getContentPane().add(lblTablas);
		
		JLabel lblIngreseElNumero = new JLabel("Ingrese el Numero:");
		lblIngreseElNumero.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngreseElNumero.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblIngreseElNumero.setBounds(31, 72, 172, 25);
		frame.getContentPane().add(lblIngreseElNumero);
		
		textPaneTablaResult = new JTextPane();
		textPaneTablaResult.setEditable(false);
		textPaneTablaResult.setBackground(Color.WHITE);
		textPaneTablaResult.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPaneTablaResult.setBounds(223, 135, 172, 193);
		frame.getContentPane().add(textPaneTablaResult);
		
		
		comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			strRes = "";
			textPaneTablaResult.setText("");
			int iResto = 0;
			iSumaNros  = 0;
			}
		});
		String strTablas[]= new String[10];
		for(int i=0;i<10;i++){
			strTablas[i]=Integer.toString(i+1);
		}
		comboBox.setModel(new DefaultComboBoxModel(strTablas));
		comboBox.setBounds(213, 74, 58, 20);
		frame.getContentPane().add(comboBox);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int inro = Integer.parseInt((String) comboBox.getSelectedItem());
				for(int i = 1;i<11;i++){
					Integer iRes = inro*i;
					int iResto = Math.abs(iRes%2-1);//Este es el famoso numero magico
					strRes+=Integer.toString(inro)+" x "+Integer.toString(i)+" = "+inro*i+"\n";
					iSumaNros+=iResto*iRes;					
				}
				textPaneTablaResult.setText(strRes);
				lblSuma.setText(String.valueOf(iSumaNros));
			}
		});
		btnCalcular.setBounds(172, 349, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		lblLaSumaDe = new JLabel("La suma de los Numeros pares es:");
		lblLaSumaDe.setBounds(10, 120, 236, 14);
		frame.getContentPane().add(lblLaSumaDe);
		
		lblSuma = new JLabel("");
		lblSuma.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSuma.setHorizontalAlignment(SwingConstants.CENTER);
		lblSuma.setBounds(10, 145, 163, 61);
		frame.getContentPane().add(lblSuma);
		
		
	}
}
