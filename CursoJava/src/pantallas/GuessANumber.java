package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GuessANumber {

	private JFrame frame;
	private JTextField textField;
	private JLabel lblIcono;
	private JLabel lblRes;
	private JButton btnAceptar;
	private JLabel lblNewLabel;
	private JLabel lblIntentos;
	private JButton btnLimpiar;
	private JLabel lblNroelegido;
	private JLabel lblElegido;
	private int cantidadIntentos; //Cuenta nro intentos

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuessANumber window = new GuessANumber();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GuessANumber() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblGuessANumber = new JLabel("Guess A Number");
		lblGuessANumber.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblGuessANumber.setBounds(106, 11, 221, 37);
		frame.getContentPane().add(lblGuessANumber);
		
		JLabel lblElijaUnNumero = new JLabel("Elija un numero entre 1 y 99:");
		lblElijaUnNumero.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblElijaUnNumero.setBounds(10, 69, 261, 25);
		frame.getContentPane().add(lblElijaUnNumero);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setBounds(281, 66, 86, 31);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
	
		lblRes = new JLabel("");
		lblRes.setVisible(false);
		lblRes.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblRes.setBounds(10, 150, 151, 25);
		frame.getContentPane().add(lblRes);
		
		lblIcono = new JLabel("");
		lblIcono.setHorizontalAlignment(SwingConstants.CENTER);
		lblIcono.setIcon(new ImageIcon(GuessANumber.class.getResource("")));
		lblIcono.setBounds(178, 138, 48, 48);
		frame.getContentPane().add(lblIcono);
		
		lblNewLabel = new JLabel("Cantidad de Intentos:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(10, 102, 194, 25);
		frame.getContentPane().add(lblNewLabel);
		
		lblNroelegido = new JLabel("Numero Elegido:");
		lblNroelegido.setVisible(false);
		lblNroelegido.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNroelegido.setBounds(10, 218, 148, 25);
		frame.getContentPane().add(lblNroelegido);
		
		lblElegido = new JLabel("");
		lblElegido.setVisible(false);
		lblElegido.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblElegido.setBounds(164, 218, 104, 25);
		frame.getContentPane().add(lblElegido);
		int inroelegido = (int) Math.floor(Math.random()*100);
		lblElegido.setText(String.valueOf(inroelegido));

		
		lblIntentos = new JLabel("");
		lblIntentos.setHorizontalAlignment(SwingConstants.CENTER);
		lblIntentos.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblIntentos.setBounds(281, 102, 86, 25);
		frame.getContentPane().add(lblIntentos);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int imivalor = Integer.parseInt(textField.getText());
				int inro = Integer.parseInt(lblElegido.getText());
					
				if(imivalor>99 || imivalor<1){
					lblRes.setText("Error");
					lblRes.setFont(new Font("Tahoma", Font.PLAIN, 30));
					lblRes.setVisible(true);
					lblIcono.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/fallido_32px.png")));
				}else if(inro>imivalor){
					cantidadIntentos++;
					lblRes.setText("Mas Arriba");
					lblIcono.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/dedoArriba_32px.png")));
					lblIntentos.setText(String.valueOf(cantidadIntentos));
					lblRes.setVisible(true);
				}else if(inro<imivalor){
					cantidadIntentos++;
					lblRes.setText("Mas Abajo");
					lblIcono.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/dedoAbajo_32px.png")));
					lblIntentos.setText(String.valueOf(cantidadIntentos));
					lblRes.setVisible(true);
				}else{
					lblRes.setText("Numero Correcto");
					cantidadIntentos++;
					lblElegido.setVisible(true);
					lblNroelegido.setVisible(true);
					lblIcono.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/correcto_32px.png")));
					lblIntentos.setText(String.valueOf(cantidadIntentos));
					lblRes.setVisible(true);
					}

			}
		});
		btnAceptar.setBounds(281, 163, 89, 23);
		frame.getContentPane().add(btnAceptar);
				
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblIcono.setIcon(new ImageIcon(GuessANumber.class.getResource("")));
				cantidadIntentos = 0;
				lblElegido.setText("");
				textField.setText("");
				lblIntentos.setText("");
				lblElegido.setVisible(false);
				lblNroelegido.setVisible(false);
				lblRes.setText("");
						
		}});
		btnLimpiar.setBounds(281, 197, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		
		
	}
}
