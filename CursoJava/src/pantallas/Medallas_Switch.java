package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Medallas_Switch {

	private JFrame frame;
	private JTextField txtPos;
	private JLabel lblIcono;
	private JButton btnCalcular;
	private JButton btnLimpiar;
	private JLabel lblPos;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Medallas_Switch window = new Medallas_Switch();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Medallas_Switch() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPosiciones = new JLabel("Posiciones");
		lblPosiciones.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblPosiciones.setBounds(159, 11, 115, 31);
		frame.getContentPane().add(lblPosiciones);
		
		JLabel lblPosicinObtenida = new JLabel("Posici\u00F3n Obtenida:");
		lblPosicinObtenida.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblPosicinObtenida.setBounds(57, 83, 166, 25);
		frame.getContentPane().add(lblPosicinObtenida);
		
		txtPos = new JTextField();
		txtPos.setHorizontalAlignment(SwingConstants.CENTER);
		txtPos.setFont(new Font("Tahoma", Font.PLAIN, 20));
		txtPos.setBounds(246, 80, 28, 31);
		frame.getContentPane().add(txtPos);
		txtPos.setColumns(10);
		
		lblIcono = new JLabel("");
		lblIcono.setHorizontalAlignment(SwingConstants.CENTER);
		lblIcono.setIcon(new ImageIcon(Medallas_Switch.class.getResource("")));
		lblIcono.setBounds(185, 119, 64, 64);
		frame.getContentPane().add(lblIcono);
		
		lblPos = new JLabel("");
		lblPos.setHorizontalAlignment(SwingConstants.CENTER);
		lblPos.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblPos.setBounds(114, 207, 206, 31);
		frame.getContentPane().add(lblPos);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				int intPos = Integer.parseInt(txtPos.getText());
				
				if(intPos <= 0){
					lblPos.setText("Ingrese un numero valido");
					lblPos.setFont(new Font("Tahoma", Font.PLAIN, 15));
					lblIcono.setIcon(new ImageIcon(Medallas_Switch.class.getResource("/iconos/fallido_32px.png")));
				}else if(intPos <= 3){
				switch (intPos) {
				case 1:
					lblPos.setText("Primer Lugar");
					lblIcono.setIcon(new ImageIcon(Medallas_Switch.class.getResource("/iconos/medalla_oro_64px.png")));
					break;
				case 2:
					lblPos.setText("Segundo Lugar");
					lblIcono.setIcon(new ImageIcon(Medallas_Switch.class.getResource("/iconos/medalla_plata_64px.png")));
					break;
				case 3:
					lblPos.setText("Tercer Lugar");
					lblIcono.setIcon(new ImageIcon(Medallas_Switch.class.getResource("/iconos/medalla_bronce_64px.png")));
					break;
				}}else{
					lblPos.setText("Segu� Participando");
					lblIcono.setIcon(new ImageIcon(Medallas_Switch.class.getResource("/iconos/fantasma_32px.png")));
				}		
			}
		});
		btnCalcular.setBounds(10, 228, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblPos.setText("");
				lblIcono.setIcon(new ImageIcon(Medallas_Switch.class.getResource("")));
				txtPos.setText("");
			}
		});
		btnLimpiar.setBounds(335, 228, 89, 23);
		frame.getContentPane().add(btnLimpiar);
	}

}
