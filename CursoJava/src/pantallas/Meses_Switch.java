package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Meses_Switch {

	private JFrame frame;
	private JTextField textField;
	private JButton btnCalcular;
	private JButton btnLimpiar;
	private JLabel lblDias;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Meses_Switch window = new Meses_Switch();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Meses_Switch() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().setForeground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblDiasDelMes = new JLabel("Dias del Mes");
		lblDiasDelMes.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblDiasDelMes.setBounds(147, 11, 139, 31);
		frame.getContentPane().add(lblDiasDelMes);
		
		JLabel lblMes = new JLabel("Numero del Mes:");
		lblMes.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblMes.setBounds(44, 74, 151, 25);
		frame.getContentPane().add(lblMes);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField.setColumns(10);
		textField.setBounds(200, 76, 86, 20);
		frame.getContentPane().add(textField);
		
		lblDias = new JLabel("");
		lblDias.setHorizontalAlignment(SwingConstants.CENTER);
		lblDias.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblDias.setBounds(10, 110, 414, 48);
		frame.getContentPane().add(lblDias);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int iMes = Integer.parseInt(textField.getText());
				
				if(iMes > 12 || iMes < 1){
					lblDias.setText("Ingrese un numero entre 1 y 12");
				}else{
				switch (iMes) {
				case 1:
					lblDias.setText("Enero tiene 31 d�as");
					break;
				case 2:
					lblDias.setText("Febrero tiene 28 d�as");
					break;
				case 3:
					lblDias.setText("Marzo tiene 31 d�as");
					break;
				case 4:
					lblDias.setText("Abril tiene 30 d�as");
					break;
				case 5:
					lblDias.setText("Mayo tiene 31 d�as");
					break;
				case 6:
					lblDias.setText("Junio tiene 30 d�as");
					break;
				case 7:
					lblDias.setText("Julio tiene 31 d�as");
					break;
				case 8:
					lblDias.setText("Agosto tiene 31 d�as");
					break;
				case 9:
					lblDias.setText("Septiembre tiene 30 d�as");
					break;
				case 10:
					lblDias.setText("Octubre tiene 31 d�as");
					break;
				case 11:
					lblDias.setText("Noviembre tiene 30 d�as");
					break;
				case 12:
					lblDias.setText("Diciembre tiene 31 d�as");
					break;
				}
			}
		}});
		btnCalcular.setBounds(85, 175, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
			lblDias.setText("");
			textField.setText("");
			
			}
		});
		btnLimpiar.setBounds(259, 175, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		

	}
}
