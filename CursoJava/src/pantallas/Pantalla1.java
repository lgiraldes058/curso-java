package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Pantalla1 {

	private JFrame frame;
	private JTextField txtCarga1;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla1 window = new Pantalla1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.YELLOW);
		frame.setBounds(100, 100, 571, 331);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCalculoDeLa = new JLabel("Calculo de la fuerza de atracc\u00F3n o repulsi\u00F3n");
		lblCalculoDeLa.setForeground(Color.BLUE);
		lblCalculoDeLa.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 24));
		lblCalculoDeLa.setBounds(10, 21, 519, 29);
		frame.getContentPane().add(lblCalculoDeLa);
		
		JLabel lblCargaQ = new JLabel("Carga q1");
		lblCargaQ.setForeground(Color.BLUE);
		lblCargaQ.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblCargaQ.setBounds(38, 88, 100, 20);
		frame.getContentPane().add(lblCargaQ);
		
		txtCarga1 = new JTextField();
		txtCarga1.setBounds(155, 90, 86, 20);
		frame.getContentPane().add(txtCarga1);
		txtCarga1.setColumns(10);
		
		JLabel lblCargaQ_1 = new JLabel("Carga q2");
		lblCargaQ_1.setForeground(Color.BLUE);
		lblCargaQ_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		lblCargaQ_1.setBounds(38, 125, 73, 20);
		frame.getContentPane().add(lblCargaQ_1);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(155, 127, 86, 20);
		frame.getContentPane().add(textField);
		
		JLabel lblDistancia = new JLabel("distancia");
		lblDistancia.setForeground(Color.BLUE);
		lblDistancia.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		lblDistancia.setBounds(38, 156, 80, 40);
		frame.getContentPane().add(lblDistancia);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(155, 158, 86, 20);
		frame.getContentPane().add(textField_1);
		
		JButton btnCalcular = new JButton("calcular");
		btnCalcular.setForeground(Color.BLUE);
		btnCalcular.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		btnCalcular.setBounds(155, 211, 115, 35);
		frame.getContentPane().add(btnCalcular);
		
		JLabel lblResultCalculo = new JLabel("Resultado");
		lblResultCalculo.setForeground(Color.BLUE);
		lblResultCalculo.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 24));
		lblResultCalculo.setBounds(322, 93, 140, 29);
		frame.getContentPane().add(lblResultCalculo);
		
		JLabel lblResultado = new JLabel("cuenta");
		lblResultado.setForeground(Color.WHITE);
		lblResultado.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 24));
		lblResultado.setOpaque(true);
		lblResultado.setBackground(Color.BLUE);
		lblResultado.setBounds(322, 148, 135, 42);
		frame.getContentPane().add(lblResultado);
	}
}
