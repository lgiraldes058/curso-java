package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class Pantalla10 {

	private JFrame frame;
	private JLabel lblIcono;
	private JComboBox comboBox;
	private JLabel lblPPT;
	private JButton btnJugar;
	private JButton btnLimpiar;
	private JLabel lblIcono3;
	private JLabel lblIcono2;
	private JLabel lblGanaste;
	private JLabel lblLaMia;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla10 window = new Pantalla10();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla10() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblpiedraPapelO = new JLabel("\u00BFPiedra, Papel o Tijera?");
		lblpiedraPapelO.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblpiedraPapelO.setBounds(60, 11, 313, 37);
		frame.getContentPane().add(lblpiedraPapelO);
		
		JLabel lblTuEleccion = new JLabel("Tu eleccion:");
		lblTuEleccion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblTuEleccion.setBounds(90, 84, 78, 19);
		frame.getContentPane().add(lblTuEleccion);
		
		lblIcono = new JLabel("");
		lblIcono.setIcon(new ImageIcon(Pantalla10.class.getResource("")));
		lblIcono.setBounds(279, 77, 32, 32);
		frame.getContentPane().add(lblIcono);
		
		lblPPT = new JLabel("");
		lblPPT.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPPT.setBounds(178, 171, 78, 19);
		frame.getContentPane().add(lblPPT);
		
		lblIcono2 = new JLabel("");
		lblIcono2.setIcon(new ImageIcon(Pantalla10.class.getResource("")));
		lblIcono2.setBounds(279, 164, 32, 32);
		frame.getContentPane().add(lblIcono2);
		
		lblGanaste = new JLabel("");
		lblGanaste.setHorizontalAlignment(SwingConstants.CENTER);
		lblGanaste.setFont(new Font("Tahoma", Font.PLAIN, 23));
		lblGanaste.setBounds(165, 124, 104, 28);
		frame.getContentPane().add(lblGanaste);
		
		lblIcono3 = new JLabel("");
		lblIcono3.setIcon(new ImageIcon(Pantalla10.class.getResource("")));
		lblIcono3.setBounds(279, 124, 32, 32);
		frame.getContentPane().add(lblIcono3);
		
		comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String strPPT = comboBox.getSelectedItem().toString();
				
				if(strPPT.equals("Piedra")){
					lblIcono.setIcon(new ImageIcon(Pantalla10.class.getResource("/iconos/piedra_32px.png")));
				} else if(strPPT.equals("Papel")){
					lblIcono.setIcon(new ImageIcon(Pantalla10.class.getResource("/iconos/papel_32px.png")));
				} else if(strPPT.equals("Tijera")){
					lblIcono.setIcon(new ImageIcon(Pantalla10.class.getResource("/iconos/tijera_32px.png")));
				} else{
					lblIcono.setIcon(new ImageIcon(Pantalla10.class.getResource("")));
				}
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"", "Piedra", "Papel", "Tijera"}));
		comboBox.setBounds(178, 83, 78, 20);
		frame.getContentPane().add(comboBox);
		
		btnJugar = new JButton("Jugar");
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				String strPPT = comboBox.getSelectedItem().toString();
				int iValor = (int) Math.floor(Math.random()*3+1);

				if(iValor == 1){
					lblPPT.setText("Piedra");
					lblIcono2.setIcon(new ImageIcon(Pantalla10.class.getResource("/iconos/piedra_32px.png")));
						if(strPPT.equals("Piedra")){
							lblGanaste.setText("EMPATE");
							lblIcono3.setIcon(new ImageIcon(Pantalla10.class.getResource("")));
						}else if(strPPT.equals("Papel")){
							lblGanaste.setText("GANASTE");
							lblIcono3.setIcon(new ImageIcon(Pantalla10.class.getResource("/iconos/correcto_32px.png")));
						}else if(strPPT.equals("Tijera")){
							lblGanaste.setText("PERDISTE");
							lblIcono3.setIcon(new ImageIcon(Pantalla10.class.getResource("/iconos/fallido2_32px.png")));
						}else{	
							lblPPT.setText("");
							lblIcono2.setIcon(new ImageIcon(Pantalla10.class.getResource("")));
						}
						
				}else if(iValor == 2){
					lblIcono2.setIcon(new ImageIcon(Pantalla10.class.getResource("/iconos/papel_32px.png")));
					lblPPT.setText("Papel");
						if(strPPT.equals("Piedra")){
							lblGanaste.setText("PERDISTE");
							lblIcono3.setIcon(new ImageIcon(Pantalla10.class.getResource("/iconos/fallido2_32px.png")));
						}else if(strPPT.equals("Papel")){
							lblGanaste.setText("EMPATE");
							lblIcono3.setIcon(new ImageIcon(Pantalla10.class.getResource("")));
						}else if(strPPT.equals("Tijera")){
							lblGanaste.setText("GANASTE");
							lblIcono3.setIcon(new ImageIcon(Pantalla10.class.getResource("/iconos/correcto_32px.png")));
						}else{	
							lblPPT.setText("");
							lblIcono2.setIcon(new ImageIcon(Pantalla10.class.getResource("")));
						}
						
				}else if(iValor == 3){
					lblPPT.setText("Tijera");
					lblIcono2.setIcon(new ImageIcon(Pantalla10.class.getResource("/iconos/tijera_32px.png")));
						if(strPPT.equals("Piedra")){
							lblGanaste.setText("GANASTE");
							lblIcono3.setIcon(new ImageIcon(Pantalla10.class.getResource("/iconos/correcto_32px.png")));
						}else if(strPPT.equals("Papel")){
							lblGanaste.setText("PERDISTE");
							lblIcono3.setIcon(new ImageIcon(Pantalla10.class.getResource("/iconos/fallido2_32px.png")));
						}else if(strPPT.equals("Tijera")){
							lblGanaste.setText("EMPATE");
							lblIcono3.setIcon(new ImageIcon(Pantalla10.class.getResource("")));
						}else{	
							lblPPT.setText("");
							lblIcono2.setIcon(new ImageIcon(Pantalla10.class.getResource("")));
						}

				
			}}}
		
			);
		btnJugar.setBounds(172, 202, 89, 23);
		frame.getContentPane().add(btnJugar);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				lblPPT.setText("");
				lblIcono.setIcon(new ImageIcon(Pantalla10.class.getResource("")));
				lblIcono2.setIcon(new ImageIcon(Pantalla10.class.getResource("")));
				lblIcono3.setIcon(new ImageIcon(Pantalla10.class.getResource("")));
				lblGanaste.setText("");
				comboBox.setModel(new DefaultComboBoxModel(new String[] {"", "Piedra", "Papel", "Tijera"}));
			}
		});
		btnLimpiar.setBounds(172, 228, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		
		lblLaMia = new JLabel("La mia:");
		lblLaMia.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblLaMia.setBounds(120, 171, 48, 19);
		frame.getContentPane().add(lblLaMia);
		
	}
}
