package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla2 {

	private JFrame frame;
	private JLabel lblByteMin;
	private JLabel lblByteMax;
	private JLabel lblTiposDeDatos;
	private JLabel lblMnimo;
	private JLabel lblMximo;
	private JLabel lblByte;
	private JLabel lblIcon;
	private JLabel lblLong;
	private JLabel lblShortMin;
	private JLabel lblShortMax;
	private JLabel lblIntMin;
	private JLabel lblIntMax;
	private JLabel lblLongMin;
	private JLabel lblLongMax;
	private JLabel lblShort;
	private JLabel lblInt;
	private JButton btnLimpiar;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla2 window = new Pantalla2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		lblTiposDeDatos = new JLabel("Tipo de Datos:");
		lblTiposDeDatos.setFont(new Font("Arial", Font.PLAIN, 16));
		lblTiposDeDatos.setBounds(10, 11, 134, 24);
		frame.getContentPane().add(lblTiposDeDatos);
		
		lblMnimo = new JLabel("M\u00EDnimo");
		lblMnimo.setFont(new Font("Arial", Font.PLAIN, 16));
		lblMnimo.setBounds(192, 14, 50, 19);
		frame.getContentPane().add(lblMnimo);
		
		lblMximo = new JLabel("M\u00E1ximo");
		lblMximo.setFont(new Font("Arial", Font.PLAIN, 16));
		lblMximo.setBounds(348, 14, 55, 19);
		frame.getContentPane().add(lblMximo);
		
		lblByte = new JLabel("byte");
		lblByte.setFont(new Font("Arial", Font.PLAIN, 16));
		lblByte.setBounds(10, 44, 50, 19);
		frame.getContentPane().add(lblByte);
		
		lblByteMin = new JLabel("");
		lblByteMin.setBackground(Color.LIGHT_GRAY);
		lblByteMin.setOpaque(true);
		lblByteMin.setFont(new Font("Arial", Font.PLAIN, 16));
		lblByteMin.setBounds(192, 44, 97, 19);
		frame.getContentPane().add(lblByteMin);
		
		lblByteMax = new JLabel("");
		lblByteMax.setBackground(Color.LIGHT_GRAY);
		lblByteMax.setForeground(Color.BLACK);
		lblByteMax.setOpaque(true);
		lblByteMax.setFont(new Font("Arial", Font.PLAIN, 16));
		lblByteMax.setBounds(326, 44, 97, 19);
		frame.getContentPane().add(lblByteMax);
		
		lblIcon = new JLabel("");
		lblIcon.setToolTipText("Soy el mejor...!!!");
		lblIcon.setIcon(new ImageIcon(Pantalla2.class.getResource("/iconos/correcto2_32px.png")));
		lblIcon.setBounds(126, 3, 32, 32);
		frame.getContentPane().add(lblIcon);
		
		lblShort = new JLabel("Short");
		lblShort.setFont(new Font("Arial", Font.PLAIN, 16));
		lblShort.setBounds(10, 85, 50, 19);
		frame.getContentPane().add(lblShort);
		
		lblInt = new JLabel("Int");
		lblInt.setFont(new Font("Arial", Font.PLAIN, 16));
		lblInt.setBounds(10, 129, 50, 19);
		frame.getContentPane().add(lblInt);
		
		lblLong = new JLabel("Long");
		lblLong.setFont(new Font("Arial", Font.PLAIN, 16));
		lblLong.setBounds(10, 171, 50, 19);
		frame.getContentPane().add(lblLong);
	
		lblShortMin = new JLabel("");
		lblShortMin.setOpaque(true);
		lblShortMin.setFont(new Font("Arial", Font.PLAIN, 16));
		lblShortMin.setBackground(Color.LIGHT_GRAY);
		lblShortMin.setBounds(192, 85, 97, 19);
		frame.getContentPane().add(lblShortMin);
		
		lblShortMax = new JLabel("");
		lblShortMax.setOpaque(true);
		lblShortMax.setForeground(Color.BLACK);
		lblShortMax.setFont(new Font("Arial", Font.PLAIN, 16));
		lblShortMax.setBackground(Color.LIGHT_GRAY);
		lblShortMax.setBounds(326, 85, 97, 19);
		frame.getContentPane().add(lblShortMax);
		
		lblIntMin = new JLabel("");
		lblIntMin.setOpaque(true);
		lblIntMin.setFont(new Font("Arial", Font.PLAIN, 16));
		lblIntMin.setBackground(Color.LIGHT_GRAY);
		lblIntMin.setBounds(192, 129, 97, 19);
		frame.getContentPane().add(lblIntMin);
		
		lblIntMax = new JLabel("");
		lblIntMax.setOpaque(true);
		lblIntMax.setForeground(Color.BLACK);
		lblIntMax.setFont(new Font("Arial", Font.PLAIN, 16));
		lblIntMax.setBackground(Color.LIGHT_GRAY);
		lblIntMax.setBounds(326, 129, 97, 19);
		frame.getContentPane().add(lblIntMax);
		
		lblLongMin = new JLabel("");
		lblLongMin.setOpaque(true);
		lblLongMin.setFont(new Font("Arial", Font.PLAIN, 16));
		lblLongMin.setBackground(Color.LIGHT_GRAY);
		lblLongMin.setBounds(192, 171, 97, 19);
		frame.getContentPane().add(lblLongMin);
		
		lblLongMax = new JLabel("");
		lblLongMax.setOpaque(true);
		lblLongMax.setForeground(Color.BLACK);
		lblLongMax.setFont(new Font("Arial", Font.PLAIN, 16));
		lblLongMax.setBackground(Color.LIGHT_GRAY);
		lblLongMax.setBounds(326, 171, 97, 19);
		frame.getContentPane().add(lblLongMax);
	
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				byte bmin=Byte.MIN_VALUE;
				byte bmax=(byte)(Math.pow(2, 7)-1);
				short smin=Short.MIN_VALUE;
				short smax=Short.MAX_VALUE;
				int imax=Integer.MAX_VALUE;
				int imin=Integer.MIN_VALUE;
				long lmin=Long.MIN_VALUE;
				long lmax=Long.MAX_VALUE;
				
				String strBmin = Byte.toString(bmin);
				
				lblByteMin.setText(strBmin);
				lblByteMax.setText(Byte.toString(bmax));
				lblShortMin.setText(Short.toString(smin));
				lblShortMax.setText(Short.toString(smax));
				lblIntMin.setText(Integer.toString(imin));
				lblIntMax.setText(Integer.toString(imax));
				lblLongMin.setText(Long.toString(lmin));
				lblLongMax.setText(Long.toString(lmax));
			}
		});
		btnCalcular.setBounds(10, 213, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblByteMin.setText("");
				lblByteMax.setText("");
				lblShortMin.setText("");
				lblShortMax.setText("");
				lblIntMin.setText("");
				lblIntMax.setText("");
				lblLongMin.setText("");
				lblLongMax.setText("");
			}
		});
		btnLimpiar.setBounds(334, 213, 89, 23);
		frame.getContentPane().add(btnLimpiar);
	}
}
