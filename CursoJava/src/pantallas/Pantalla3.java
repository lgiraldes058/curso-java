package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ImageIcon;

public class Pantalla3 {

	private JFrame frame;
	private JTextField textNota2;
	private JTextField textNota1;
	private JTextField textNota3;
	private JLabel lblProm;
	private JLabel lblImagen;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla3 window = new Pantalla3();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla3() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPromedio = new JLabel("Promedio de Notas");
		lblPromedio.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblPromedio.setBounds(116, 11, 202, 29);
		frame.getContentPane().add(lblPromedio);
		
		JLabel lblNota = new JLabel("Nota1:");
		lblNota.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNota.setBounds(39, 46, 60, 25);
		frame.getContentPane().add(lblNota);
		
		JLabel lblNota_1 = new JLabel("Nota2:");
		lblNota_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNota_1.setBounds(39, 117, 60, 25);
		frame.getContentPane().add(lblNota_1);
		
		JLabel lblNota_2 = new JLabel("Nota3:");
		lblNota_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNota_2.setBounds(39, 188, 60, 25);
		frame.getContentPane().add(lblNota_2);
		
		textNota2 = new JTextField();
		textNota2.setBounds(143, 120, 86, 20);
		frame.getContentPane().add(textNota2);
		textNota2.setColumns(10);
		
		textNota1 = new JTextField();
		textNota1.setColumns(10);
		textNota1.setBounds(143, 50, 86, 20);
		frame.getContentPane().add(textNota1);
		
		textNota3 = new JTextField();
		textNota3.setColumns(10);
		textNota3.setBounds(143, 190, 86, 20);
		frame.getContentPane().add(textNota3);
		
		JLabel lblPromedio_1 = new JLabel("Promedio:");
		lblPromedio_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblPromedio_1.setBounds(285, 81, 91, 25);
		frame.getContentPane().add(lblPromedio_1);
		
		lblProm = new JLabel("");
		lblProm.setOpaque(true);
		lblProm.setBackground(Color.GRAY);
		lblProm.setForeground(Color.BLACK);
		lblProm.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblProm.setBounds(285, 117, 91, 25);
		frame.getContentPane().add(lblProm);

		lblImagen = new JLabel("");
		lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/javax/swing/plaf/metal/icons/Question.gif")));
		lblImagen.setBounds(392, 110, 32, 32);
		frame.getContentPane().add(lblImagen);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//obtengo los 3 valores de las notas
				float fnota1 = Float.parseFloat(textNota1.getText());
				float fnota2 = Float.parseFloat(textNota2.getText());
				float fnota3 = Float.parseFloat(textNota3.getText());
				
				float fprom = ((fnota1+fnota2+fnota3)/3);
				
				//con esto hago que dependiendo del promedio ponga distintos colores de fondo
				
				if(fprom>=7){
					lblProm.setText(Float.toString(fprom));
					lblProm.setBackground(Color.green);
					lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/correcto_32px.png")));
				}
				else{
					lblProm.setText(Float.toString(fprom));
					lblProm.setBackground(Color.red);
					lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/fallido2_32px.png")));
				}
			
			}
		});
		btnCalcular.setBounds(239, 188, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				textNota1.setText("");
				textNota2.setText("");
				textNota3.setText("");
				
				lblProm.setText("");
				
				lblProm.setBackground(Color.gray);
				
				lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/javax/swing/plaf/metal/icons/Question.gif")));
			}
		});
		btnLimpiar.setBounds(335, 188, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/papel_32px.png")));
		label.setBounds(328, 11, 32, 32);
		frame.getContentPane().add(label);
		

	}

}
