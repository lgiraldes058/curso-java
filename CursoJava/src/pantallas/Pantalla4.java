package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class Pantalla4 {

	private JFrame frame;
	private JTextField textNro;
	private JLabel lblPoI;
	private JButton btnCalcular;
	private JButton btnLimpiar;
	private JLabel lblIcono;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla4 window = new Pantalla4();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla4() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setForeground(Color.WHITE);
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblparOImpar = new JLabel("\u00BFPar o Impar?");
		lblparOImpar.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblparOImpar.setBounds(139, 11, 156, 31);
		frame.getContentPane().add(lblparOImpar);
		
		JLabel lblNmero = new JLabel("N\u00FAmero:");
		lblNmero.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNmero.setBounds(124, 72, 77, 25);
		frame.getContentPane().add(lblNmero);
		
		textNro = new JTextField();
		textNro.setBounds(211, 74, 86, 20);
		frame.getContentPane().add(textNro);
		textNro.setColumns(10);
		
		lblPoI = new JLabel("");
		lblPoI.setHorizontalAlignment(SwingConstants.CENTER);
		lblPoI.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblPoI.setBounds(119, 130, 196, 55);
		frame.getContentPane().add(lblPoI);
		
		
		lblIcono = new JLabel("");
		lblIcono.setIcon(new ImageIcon(Pantalla4.class.getResource("/com/sun/java/swing/plaf/windows/icons/image-delayed.png")));
		lblIcono.setBounds(201, 196, 32, 32);
		frame.getContentPane().add(lblIcono);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int inro = Integer.parseInt(textNro.getText());
				
				if(inro == 0){
					lblPoI.setText("Ingrese otro Numero");
					lblIcono.setIcon(new ImageIcon(Pantalla4.class.getResource("/iconos/fallido2_32px.png")));
					lblPoI.setFont(new Font("Tahoma", Font.PLAIN, 20));
				}
				else
					if(inro % 2 == 0){
						lblPoI.setText("ES PAR");
					}
					else{
						lblPoI.setText("ES IMPAR");
				}
			}
		});
		btnCalcular.setBounds(10, 150, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblPoI.setText("");
				textNro.setText("");
				lblIcono.setIcon(new ImageIcon(Pantalla4.class.getResource("/com/sun/java/swing/plaf/windows/icons/image-delayed.png")));
				lblPoI.setFont(new Font("Tahoma", Font.PLAIN, 30));
			}
		});
		btnLimpiar.setBounds(335, 150, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		

	}

}
