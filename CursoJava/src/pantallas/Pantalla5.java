package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Pantalla5 {

	private JFrame frame;
	private JTextField textField;
	private JButton btnCalcular;
	private JButton btnLimpiar;
	private JLabel lblDias;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla5 window = new Pantalla5();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla5() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().setForeground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblDiasDelMes = new JLabel("Dias del Mes");
		lblDiasDelMes.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblDiasDelMes.setBounds(147, 11, 139, 31);
		frame.getContentPane().add(lblDiasDelMes);
		
		JLabel lblMes = new JLabel("Mes:");
		lblMes.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblMes.setBounds(132, 74, 42, 25);
		frame.getContentPane().add(lblMes);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField.setColumns(10);
		textField.setBounds(200, 76, 86, 20);
		frame.getContentPane().add(textField);
		
		lblDias = new JLabel("");
		lblDias.setHorizontalAlignment(SwingConstants.CENTER);
		lblDias.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblDias.setBounds(97, 110, 239, 48);
		frame.getContentPane().add(lblDias);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String strMes = textField.getText();
				
					if(strMes.equals("Febrero")){
						lblDias.setText(strMes + " tiene 28 d�as");
				}
					else if(strMes.equals("Abril") || strMes.equals("Junio") || strMes.equals("Septiembre") || strMes.equals("Noviembre") ){
						lblDias.setText(strMes + " tiene 30 d�as");
				}
					else{
						lblDias.setText(strMes + " tiene 31 d�as");
				}
			}
		});
		btnCalcular.setBounds(85, 175, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
			lblDias.setText("");
			textField.setText("");
			
			}
		});
		btnLimpiar.setBounds(259, 175, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		

	}
}
