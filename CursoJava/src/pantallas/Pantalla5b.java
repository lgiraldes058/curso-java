package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
public class Pantalla5b {

	private JFrame frame;
	private JLabel lblDias;
	private JComboBox comboBoxMes;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla5b window = new Pantalla5b();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla5b() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().setForeground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblDiasDelMes = new JLabel("Dias del Mes");
		lblDiasDelMes.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblDiasDelMes.setBounds(147, 11, 139, 31);
		frame.getContentPane().add(lblDiasDelMes);
		
		JLabel lblMes = new JLabel("Mes:");
		lblMes.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblMes.setBounds(113, 74, 42, 25);
		frame.getContentPane().add(lblMes);
		
		lblDias = new JLabel("");
		lblDias.setHorizontalAlignment(SwingConstants.CENTER);
		lblDias.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblDias.setBounds(97, 110, 239, 48);
		frame.getContentPane().add(lblDias);
		
		comboBoxMes = new JComboBox();
		comboBoxMes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String strMes = comboBoxMes.getSelectedItem().toString();
				
				if(strMes.equals("Febrero")){
					lblDias.setText(strMes + " tiene 28 d�as");
			}
				else if(strMes.equals("Abril") || strMes.equals("Junio") || strMes.equals("Septiembre") || strMes.equals("Noviembre") ){
					lblDias.setText(strMes + " tiene 30 d�as");
			}
				else if(strMes.equals("Enero") || strMes.equals("Marzo") || strMes.equals("Mayo") || strMes.equals("Julio") || strMes.equals("Agosto") || strMes.equals("Octubre") || strMes.equals("Diciembre")){
					lblDias.setText(strMes + " tiene 31 d�as");
			}	else{
					lblDias.setText("");
			}
		}
		});
		comboBoxMes.setModel(new DefaultComboBoxModel(new String[] {"", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"}));
		comboBoxMes.setBounds(160, 76, 114, 20);
		frame.getContentPane().add(comboBoxMes);
		
	}
}
