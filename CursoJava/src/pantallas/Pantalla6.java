package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla6 {

	private JFrame frame;
	private JTextField textCat;
	private JLabel lblIcono;
	private JLabel lblCat;
	private JButton btnCalcular;
	private JButton btnLimpiar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla6 window = new Pantalla6();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla6() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCategoras = new JLabel("Categor\u00EDas");
		lblCategoras.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblCategoras.setBounds(159, 11, 116, 31);
		frame.getContentPane().add(lblCategoras);
		
		JLabel lblCategora = new JLabel("Categor\u00EDa:");
		lblCategora.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblCategora.setBounds(124, 77, 66, 19);
		frame.getContentPane().add(lblCategora);
		
		textCat = new JTextField();
		textCat.setBounds(200, 78, 86, 20);
		frame.getContentPane().add(textCat);
		textCat.setColumns(10);
		
		lblCat = new JLabel("");
		lblCat.setHorizontalAlignment(SwingConstants.CENTER);
		lblCat.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblCat.setBounds(165, 159, 104, 37);
		frame.getContentPane().add(lblCat);
		
		lblIcono = new JLabel("");
		lblIcono.setBounds(301, 164, 32, 32);
		frame.getContentPane().add(lblIcono);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String strCat = textCat.getText();
				
				if(strCat.equals("A") || strCat.equals("a") ){
					lblCat.setText("Hijos");
					lblIcono.setIcon(new ImageIcon(Pantalla6.class.getResource("/iconos/sol_32px.png")));
				} else if(strCat.equals("B") || strCat.equals("b")){
					lblCat.setText("Padres");
					lblIcono.setIcon(new ImageIcon(Pantalla6.class.getResource("/iconos/lluvia_32px.png")));
				} else if(strCat.equals("C") || strCat.equals("c")){
					lblCat.setText("Abuelos");
					lblIcono.setIcon(new ImageIcon(Pantalla6.class.getResource("/iconos/fantasma_32px.png")));
				} else{
					lblCat.setText("ERROR");
					lblIcono.setIcon(new ImageIcon(Pantalla6.class.getResource("/iconos/fallido2_32px.png")));
				}
			}
		});
		btnCalcular.setBounds(10, 228, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblIcono.setIcon(new ImageIcon(Pantalla6.class.getResource("")));
				lblCat.setText("");
				textCat.setText("");
			}
		});
		btnLimpiar.setBounds(335, 228, 89, 23);
		frame.getContentPane().add(btnLimpiar);
	}
}
