package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla8 {

	private JFrame frame;
	private JTextField textField;
	private JLabel lblCur;
	private JButton btnLimpiar;
	private JButton btnCalcular;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla8 window = new Pantalla8();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla8() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCursos = new JLabel("Cursos");
		lblCursos.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblCursos.setBounds(173, 11, 88, 37);
		frame.getContentPane().add(lblCursos);
		
		JLabel lblCurso = new JLabel("Curso:");
		lblCurso.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblCurso.setBounds(105, 82, 57, 25);
		frame.getContentPane().add(lblCurso);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField.setBounds(174, 76, 86, 31);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		lblCur = new JLabel("");
		lblCur.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblCur.setHorizontalAlignment(SwingConstants.CENTER);
		lblCur.setBounds(137, 131, 160, 25);
		frame.getContentPane().add(lblCur);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
			int inro = Integer.parseInt(textField.getText());
			
			if(inro==0){
				lblCur.setText("Jardin de infantes");
			}else if(inro>=1 && inro<=6){
				lblCur.setText("Primaria");
			}else if(inro>=7 && inro<=12){
				lblCur.setText("Secundaria");
			}else{
				lblCur.setText("Error");
			}
			
			}
		});
		btnCalcular.setBounds(172, 179, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblCur.setText("");
			textField.setText("");
			}
		});
		btnLimpiar.setBounds(172, 213, 89, 23);
		frame.getContentPane().add(btnLimpiar);
	}
}
