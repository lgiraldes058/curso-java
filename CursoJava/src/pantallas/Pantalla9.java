package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla9 {

	private JFrame frame;
	private JTextField textN1;
	private JTextField textN2;
	private JTextField textN3;
	private JLabel lblNumero;
	private JLabel lblNumero_2;
	private JLabel lblNumero_1;
	private JLabel lblNM;
	private JButton btnLimpiar;
	private JButton btnCalcular;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla9 window = new Pantalla9();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla9() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNumeroMayor = new JLabel("Numero mayor");
		lblNumeroMayor.setHorizontalAlignment(SwingConstants.CENTER);
		lblNumeroMayor.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblNumeroMayor.setBounds(118, 11, 197, 37);
		frame.getContentPane().add(lblNumeroMayor);
		
		lblNumero = new JLabel("Numero 1:");
		lblNumero.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNumero.setBounds(118, 59, 70, 19);
		frame.getContentPane().add(lblNumero);
		
		lblNumero_2 = new JLabel("Numero 2:");
		lblNumero_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNumero_2.setBounds(118, 89, 70, 19);
		frame.getContentPane().add(lblNumero_2);
		
		lblNumero_1 = new JLabel("Numero 3:");
		lblNumero_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNumero_1.setBounds(118, 119, 70, 19);
		frame.getContentPane().add(lblNumero_1);
		
		textN1 = new JTextField();
		textN1.setBounds(198, 58, 86, 20);
		frame.getContentPane().add(textN1);
		textN1.setColumns(10);
		
		textN2 = new JTextField();
		textN2.setColumns(10);
		textN2.setBounds(198, 88, 86, 20);
		frame.getContentPane().add(textN2);
		
		textN3 = new JTextField();
		textN3.setColumns(10);
		textN3.setBounds(198, 118, 86, 20);
		frame.getContentPane().add(textN3);
		
		JLabel lblMayor = new JLabel("El mayor numero es: ");
		lblMayor.setHorizontalAlignment(SwingConstants.CENTER);
		lblMayor.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblMayor.setBounds(10, 149, 191, 25);
		frame.getContentPane().add(lblMayor);
		
		lblNM = new JLabel("");
		lblNM.setHorizontalAlignment(SwingConstants.CENTER);
		lblNM.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNM.setBounds(198, 149, 88, 25);
		frame.getContentPane().add(lblNM);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				int inro1 = Integer.parseInt(textN1.getText());
				int inro2 = Integer.parseInt(textN2.getText());
				int inro3 = Integer.parseInt(textN3.getText());
				
				if(inro1>inro2 && inro1>inro3){
					lblNM.setText(String.valueOf(inro1));
				} else if(inro2>inro1 && inro2>inro3){
					lblNM.setText(String.valueOf(inro2));
				} else if(inro3>inro2 && inro3>inro1){
					lblNM.setText(String.valueOf(inro3));
				} else if(inro1 == inro2 && inro2 == inro3){
					lblNM.setText("ninguno");
				}
			
			}
		});
		btnCalcular.setBounds(170, 192, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblNM.setText("");
			textN1.setText("");
			textN2.setText("");
			textN3.setText("");
			}
		});
		btnLimpiar.setBounds(170, 226, 89, 23);
		frame.getContentPane().add(btnLimpiar);
	}
}
