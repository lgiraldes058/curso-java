package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PruebaRandom {

	private JFrame frame;
	private JLabel lblNro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PruebaRandom window = new PruebaRandom();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PruebaRandom() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		lblNro = new JLabel("nro");
		lblNro.setBounds(181, 69, 46, 14);
		frame.getContentPane().add(lblNro);
		
		JButton btnJugar = new JButton("jugar");
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int iValor = (int) Math.floor(Math.random()*3+1);
				String valor = Integer.toString(iValor);
				lblNro.setText(valor);
			}
		});
		btnJugar.setBounds(162, 115, 89, 23);
		frame.getContentPane().add(btnJugar);
	}
}
