package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;

public class Tabla_Multiplicacion_Completa {

	private JFrame frame;
	private JButton btnCalcular;
	private JTextPane textPaneTablaResult;
	private String strRes = "";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tabla_Multiplicacion_Completa window = new Tabla_Multiplicacion_Completa();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Tabla_Multiplicacion_Completa() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 772, 415);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTablas = new JLabel("Tablas");
		lblTablas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblTablas.setBounds(334, 11, 87, 37);
		frame.getContentPane().add(lblTablas);
		
		textPaneTablaResult = new JTextPane();
		textPaneTablaResult.setEditable(false);
		textPaneTablaResult.setBackground(Color.WHITE);
		textPaneTablaResult.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPaneTablaResult.setBounds(10, 59, 736, 259);
		frame.getContentPane().add(textPaneTablaResult);
		
		final String strTabla[][] = new String[10][10];
		for(int i = 0;i<strTabla.length;i++){
			for(int j = 0;j<strTabla.length;j++){
			strTabla[i][0] = Integer.toString(i);
			strTabla[0][j] = Integer.toString(j);
			}
		}
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			for(int i = 1;i<strTabla.length;i++){
				for(int j = 1;j<strTabla.length;j++){
					strRes+="     "+Integer.toString(j)+"x"+Integer.toString(i)+"="+Integer.toString(i*j);
				}strRes+="\n";
			}
			textPaneTablaResult.setText(strRes);
			}
		});
		btnCalcular.setBounds(332, 348, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		
	}
}
