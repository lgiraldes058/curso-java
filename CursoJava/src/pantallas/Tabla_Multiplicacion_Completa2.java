package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JList;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;

public class Tabla_Multiplicacion_Completa2 {

	private JFrame frame;
	private JButton btnCalcular;
	private JList list;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tabla_Multiplicacion_Completa2 window = new Tabla_Multiplicacion_Completa2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Tabla_Multiplicacion_Completa2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 772, 415);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTablas = new JLabel("Tablas");
		lblTablas.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblTablas.setBounds(334, 11, 87, 37);
		frame.getContentPane().add(lblTablas);
		
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String strTabla[][] = new String[10][10];
				for(int i = 0; i<strTabla.length;i++){
					for(int j=0;j<strTabla.length;j++){
						strTabla[i][j] = i+" x "+j+" = "+i*j;
					}
				}
			}
		});
		btnCalcular.setBounds(332, 348, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(28, 293, 87, -204);
		frame.getContentPane().add(scrollPane);
		
		list = new JList();
		list.setFont(new Font("Tahoma", Font.PLAIN, 15));
		scrollPane.setViewportView(list);
		
		
	}
}
