package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VocaloConsonante {

	private JFrame frame;
	private JTextField textField;
	private JButton btnLimpiar;
	private JButton btnAceptar;
	private JLabel lblRes;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VocaloConsonante window = new VocaloConsonante();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VocaloConsonante() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblesUnaVocal = new JLabel("\u00BFEs una Vocal o una Consonante?");
		lblesUnaVocal.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblesUnaVocal.setBounds(29, 11, 375, 31);
		frame.getContentPane().add(lblesUnaVocal);
		
		JLabel lblIngreseLaLetra = new JLabel("Ingrese la Letra:");
		lblIngreseLaLetra.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblIngreseLaLetra.setBounds(80, 77, 108, 19);
		frame.getContentPane().add(lblIngreseLaLetra);
		
		textField = new JTextField();
		textField.setBounds(197, 76, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		lblRes = new JLabel("");
		lblRes.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblRes.setHorizontalAlignment(SwingConstants.CENTER);
		lblRes.setBounds(29, 129, 375, 31);
		frame.getContentPane().add(lblRes);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			char cletra = textField.getText().charAt(0);
			
			if(cletra == 'a' || cletra == 'e' || cletra == 'i' || cletra == 'o' || cletra == 'u' || cletra == 'A' || cletra == 'E' || cletra == 'I' || cletra == 'O' || cletra == 'U'){
				lblRes.setText(cletra+" es una vocal");
			}else{
				lblRes.setText(cletra+" no es una vocal");
			}
			
			}
		});
		btnAceptar.setBounds(172, 171, 89, 23);
		frame.getContentPane().add(btnAceptar);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			lblRes.setText("");
			textField.setText("");
			}
		});
		btnLimpiar.setBounds(172, 205, 89, 23);
		frame.getContentPane().add(btnLimpiar);
	}

}
